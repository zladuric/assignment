import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '@invoices/api-interfaces';
import { ClientListService } from '../../services/client-list.service';
import { debounceTime, tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ClientListActions, ClientListState } from '../../types';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'invoices-client-list-container',
  templateUrl: './client-list-container.component.html',
  styleUrls: ['./client-list-container.component.css'],
})
export class ClientListContainerComponent implements OnInit {
  state$: Observable<ClientListState>;
  companies: Company[] = [];
  companyName = '';

  rowCount = 0;
  pageIndex = 0;
  search = new FormControl('');

  constructor(private service: ClientListService) {}

  ngOnInit(): void {
    this.state$ = this.service.state$;
    this.search.valueChanges
      .pipe(
        debounceTime(300),
        tap(() => (this.pageIndex = 0))
      )
      .subscribe((value) =>
        this.service.dispatch(ClientListActions.LOAD_COMPANIES, { value })
      );
  }

  reloadCompanies(page: PageEvent) {
    this.service.dispatch(ClientListActions.LOAD_PAGE, page);
  }
}
