import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Company } from '@invoices/api-interfaces';
import { ClientResourceService } from './client-resource.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ClientListActions, ClientListState } from '../types';
import { initialPagingOptions } from '../types/constants';

@Injectable()
export class ClientListService {
  state$: Observable<ClientListState>;
  stateSubject: BehaviorSubject<ClientListState> = new BehaviorSubject({
    clients: [],
    currentFilter: '',
    rowCount: 0,
    paging: initialPagingOptions
  });

  constructor(private clientResource: ClientResourceService) {
    this.state$ = this.stateSubject.asObservable();
    this.dispatch(ClientListActions.LOAD_COMPANIES)
  }

  dispatch(action: ClientListActions, payload?: unknown) {
    switch (action) {
      case ClientListActions.LOAD_COMPANIES:
        this.loadCompanies();
        break;
      case ClientListActions.LOAD_PAGE:
        this.loadPage(payload);
        break;
      default:
        break;

    }
  }

  private loadCompanies(companyFilter?: string) {
    console.log(companyFilter);
    const { paging, currentFilter } = this.stateSubject.getValue();
    const filter = currentFilter || this.stateSubject.getValue().currentFilter;
    const { pageSize, pageIndex } = paging;
    this.clientResource.loadCompanies(companyFilter, pageIndex, pageSize)
      .subscribe(response => {
        this.stateSubject.next({
          clients: response.data,
          rowCount: response.count,
          paging,
          currentFilter: companyFilter,
        });
      });
  }

  private loadPage(payload: unknown) {

  }
}
