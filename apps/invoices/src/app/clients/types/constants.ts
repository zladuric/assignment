import { PagingOptions } from './index';

export const initialPagingOptions: PagingOptions = {
  pageIndex: 0,
  pageSize: 1,
  pageSizeOptions: [3, 5, 10],
  length: 0,
};
